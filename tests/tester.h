#pragma once

#include <iostream>
#include <QtCore/QDebug>
#include <QtTest/QTest>
#include <QtCore/QDateTime>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include "../src/debug.h"
#include "../src/utils/MySqlClient.h"
#include "../src/gpios/GPIOManager.h"
#include "../src/gpios/GPIOConst.h"

using namespace safex;

class CTester: public QRunnable
{
  CMySqlClient* mysql;
public:
  ///runnable routine
  virtual void run();

  ///test valves H and N
  void test_valves();

  ///test H1 module
  void test_H1();

  ///test exit bit and finish testing
  void test_exit();
public:
  //ctor and dtor are private, use init/release instead
  //
  CTester(){
    mysql = NULL;
  }
};//
