#include <QtCore/QDateTime>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include "tester.h"

using namespace safex;
//test valves H and N: check direction and value
 void CTester::test_valves(){
    GPIO::GPIOManager* gp = GPIO::GPIOManager::getInstance();

    //check directions
    int pin_vH = GPIO::GPIOConst::getInstance()->getGpioByKey("P8_3");
    TEST(GPIO::OUTPUT == gp->getDirection(pin_vH));
    //
    int pin_vN = GPIO::GPIOConst::getInstance()->getGpioByKey("P8_4");
    TEST(GPIO::OUTPUT == gp->getDirection(pin_vN));

    {//step 0, zero values
      //control and check value
      QSqlQuery q = mysql->getQuery();
      //insert query for push
      if (!q.prepare( "INSERT INTO initialize VALUES(NULL, :time, :sens_1, :sens_2, :sens_3"
          ", :e_mail, :c_shutdown, :c_stop, :valve_h, :valve_n, :save_h);")) {
        FAIL(q.lastError().text().toAscii());
      }
      //copy data to query
      q.bindValue(":time", QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
      q.bindValue(":sens_1", false);
      q.bindValue(":sens_2", false);
      q.bindValue(":sens_3", false);
      q.bindValue(":e_mail", false);
      q.bindValue(":c_shutdown", false);
      q.bindValue(":c_stop", false);
      q.bindValue(":valve_h", false);
      q.bindValue(":valve_n", false);
      q.bindValue(":save_h", false);

      if (!q.exec()) {
        FAIL(q.lastError().text().toAscii());
      }
      sleep(3);//wait for safex refresh their data
      TEST(0 == gp->getValue(pin_vH));
      TEST(0 == gp->getValue(pin_vN));
    }

    {//step 1, 0-1 values
      //control and check value
      QSqlQuery q = mysql->getQuery();
      //insert query for push
      if (!q.prepare( "INSERT INTO initialize VALUES(NULL, :time, :sens_1, :sens_2, :sens_3"
          ", :e_mail, :c_shutdown, :c_stop, :valve_h, :valve_n, :save_h);")) {
        FAIL(q.lastError().text().toAscii());
      }
      //copy data to query
      q.bindValue(":time", QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
      q.bindValue(":sens_1", false);
      q.bindValue(":sens_2", false);
      q.bindValue(":sens_3", false);
      q.bindValue(":e_mail", false);
      q.bindValue(":c_shutdown", false);
      q.bindValue(":c_stop", false);
      q.bindValue(":valve_h", true);
      q.bindValue(":valve_n", false);
      q.bindValue(":save_h", false);

      if (!q.exec()) {
        FAIL(q.lastError().text().toAscii());
      }
      sleep(3);//wait for safex refresh their data
      TEST(1 == gp->getValue(pin_vH));
      TEST(0 == gp->getValue(pin_vN));
    }
    {//step 2, 1-0 values
      //control and check value
      QSqlQuery q = mysql->getQuery();
      //insert query for push
      if (!q.prepare( "INSERT INTO initialize VALUES(NULL, :time, :sens_1, :sens_2, :sens_3"
          ", :e_mail, :c_shutdown, :c_stop, :valve_h, :valve_n, :save_h);")) {
        FAIL(q.lastError().text().toAscii());
      }
      //copy data to query
      q.bindValue(":time", QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
      q.bindValue(":sens_1", false);
      q.bindValue(":sens_2", false);
      q.bindValue(":sens_3", false);
      q.bindValue(":e_mail", false);
      q.bindValue(":c_shutdown", false);
      q.bindValue(":c_stop", false);
      q.bindValue(":valve_h", false);
      q.bindValue(":valve_n", true);
      q.bindValue(":save_h", false);

      if (!q.exec()) {
        FAIL(q.lastError().text().toAscii());
      }
      sleep(3);//wait for safex refresh their data
      TEST(0 == gp->getValue(pin_vH));
      TEST(1 == gp->getValue(pin_vN));
    }

    {//step 3, 1-1 values
      //control and check value
      QSqlQuery q = mysql->getQuery();
      //insert query for push
      if (!q.prepare( "INSERT INTO initialize VALUES(NULL, :time, :sens_1, :sens_2, :sens_3"
          ", :e_mail, :c_shutdown, :c_stop, :valve_h, :valve_n, :save_h);")) {
        FAIL(q.lastError().text().toAscii());
      }
      //copy data to query
      q.bindValue(":time", QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
      q.bindValue(":sens_1", false);
      q.bindValue(":sens_2", false);
      q.bindValue(":sens_3", false);
      q.bindValue(":e_mail", false);
      q.bindValue(":c_shutdown", false);
      q.bindValue(":c_stop", false);
      q.bindValue(":valve_h", true);
      q.bindValue(":valve_n", true);
      q.bindValue(":save_h", false);

      if (!q.exec()) {
        FAIL(q.lastError().text().toAscii());
      }
      sleep(2);//wait for safex refresh their data
      TEST(1 == gp->getValue(pin_vH));
      TEST(1 == gp->getValue(pin_vN));
    }
  }
