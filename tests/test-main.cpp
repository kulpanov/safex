
#include "tester.h"

using namespace safex;

///runnable routine
void CTester::run(){
  sleep(5);//       wait for safex has started
  mysql = CMySqlClient::getInstance();

  TEST(true == mysql->isConnected());

  test_valves();

  test_H1();

  test_exit();
}

void CTester::test_exit(){
  QSqlQuery q = mysql->getQuery();
  //insert query for push
  if (!q.prepare( "INSERT INTO initialize VALUES(NULL, :time, :sens_1, :sens_2, :sens_3"
      ", :e_mail, :c_shutdown, :c_stop, :alve_h, :alve_n, :save_h);")) {
    FAIL(q.lastError().text().toAscii());
  }
  //copy data to query
  if (!q.prepare( "INSERT INTO initialize VALUES(NULL, :time, :sens_1, :sens_2, :sens_3"
      ", :e_mail, :c_shutdown, :c_stop, :valve_h, :valve_n, :save_h);")) {
    FAIL(q.lastError().text().toAscii());
  }
  //copy data to query
  q.bindValue(":time", QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
  q.bindValue(":sens_1", false);
  q.bindValue(":sens_2", false);
  q.bindValue(":sens_3", false);
  q.bindValue(":e_mail", false);
  q.bindValue(":c_shutdown", false);
  q.bindValue(":c_stop", true);
  q.bindValue(":valve_h", false);
  q.bindValue(":valve_n", false);
  q.bindValue(":save_h", false);

  if (!q.exec()) {
    FAIL(q.lastError().text().toAscii());
  }
}

/**
 *
 */
void
main_test(char* optarg)
{
  CTester* instance = new CTester;
  QThreadPool::globalInstance()->start(instance);
  return ;
}
