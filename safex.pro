
CONFIG += debug_and_release
QT_HOME = /usr/arm-linux-gnueabihf/usr/share/qt4

QT      += sql
TEMPLATE = app
TARGET = 
DEPENDPATH += .
INCLUDEPATH += .


QMAKE_EXTRA_TARGETS += _install-release _install-debug

#QMAKE_CXXFLAGS += -std=c++11

#QMAKE_CXXFLAGS += -pg
#QMAKE_LFLAGS += -pg


# Input
HEADERS += src/utils/MySqlClient.h src/wd/module_wd.h src/debug.h src/e_stop.h src/heart_beat.h \ 
	src/module_init.h

SOURCES += src/main.cpp src/utils/MySqlClient.cpp src/wd/module_wd.cpp src/utils/debug.cpp\
	 src/gpios/GPIOConst.cpp src/gpios/GPIOManager.cpp \
	 src/e_stop.cpp src/heart_beat.cpp src/module_init.cpp
