#pragma once
#include "./utils/MySqlClient.h"
#include "gpios/GPIOManager.h"

namespace safex {
/** EmergencyStop class for stop operations.
 */
  class CEmergencyStop {
  public:
    ///constructor
    CEmergencyStop();

    //
    ~CEmergencyStop();
    ///run operations
    int execute();

  private:
    //
    GPIO::GPIOManager* gp;
    safex::CMySqlClient* mysql;
    int pin_stop;
    int pin_stop_value;
  protected:
};

}
