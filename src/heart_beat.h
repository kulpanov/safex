#pragma once
#include "./utils/MySqlClient.h"
#include "gpios/GPIOManager.h"

namespace safex {
/** Heart class for heartbeat operations.
 */
  class CHeartBeat {
  public:
    ///constructor
    CHeartBeat();

    ///heartbeat operations
    int execute();

  private:
    //
    GPIO::GPIOManager* gp;
    safex::CMySqlClient* mysql;
    //adcs fields
    QFile fs_adc4;
    QTextStream ds_adc4;
    QFile fs_adc5;
    QTextStream ds_adc5;
    QFile fs_adc6;
    QTextStream ds_adc6;

    int ain4, ain5, ain6;//adc values in mV
    //ain4=H1, ain6=H2
    int pin_alert;
    //modules routines
    int processH1();
    int processH2();
    int processH3();

  protected:
};

}
