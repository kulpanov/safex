#include <iostream>
#include <QtCore/QDebug>
#include "debug.h"
#include "module_init.h"
#include "wd/module_wd.h"
#include "utils/MySqlClient.h"
#include "heart_beat.h"
#include "e_stop.h"

//#define WITH_SELFTEST

#ifdef WITH_SELFTEST
extern int
main_test(char* optarg);
#endif

/** Parsing opts, helper routine.
 *
 * @param argc args count
 * @param argv cmd args
 * @return 0 for success
 */
int
parseOpts(int argc, char** argv);

/**
 * main: main functon
 * use: -c to create a new database
 *      -a to add some values to table
 *      -t to perform selftesting operations
 * use w/o params: just complete itself task
 *
 *Before use this programm you need check what device tree of linux is configured correctly,
 *  see devtree/AK-GPIO_test
 *  run:  "sudo setdt" for configure the device tree.
 *
 */
int
main(int argc, char** argv)
{
  //parse options
  int res = 0;
  if (EOK != (res = parseOpts(argc, argv)))
  {
    safex::CMySqlClient::release();
    exit(res>0?0:res); //return 0 if success
  }

  //perform first time initialization
  safex::CWatchDog::init();
  {
  safex::CInit initialize;
  safex::CHeartBeat heart;
  safex::CEmergencyStop e_stop;
  while (true)
  {
    sleep(1);
    initialize.execute();
    if(res != 0) break;
    res = heart.execute();
    if(res != 0) break;
    res = e_stop.execute();
    if(res != 0) break;
  } //this "{...}" for rule: last create-first delete
  }
  //cleanup
  safex::CWatchDog::release();

  return 0;
}

int
parseOpts(int argc, char** argv)
{
  int c = 0;
  while ((c = getopt(argc, argv, "act:")) != -1)
    switch (c)
      {
    case 'c':
      if ("need to create a new database")
      {
        safex::CMySqlClient* mysql = safex::CMySqlClient::getInstance();
        //TODO: Move connection params to cfg
        if (EOK != mysql->createDataBase())
        {
          VLOG(ERRR) << mysql->getLastError() << LOGN;
          return -1;
        }
        return 1;
      }
      break;
    case 'a':
      if ("need to add")
      {
        safex::CMySqlClient* mysql = safex::CMySqlClient::getInstance();
        if (0 != mysql->connect("localhost"))
        {
          VLOG(ERRR) << mysql->getLastError() << LOGN;
          return -1;
        }

        if (EOK != mysql->push())
        {
          VLOG(ERRR) << mysql->getLastError() << LOGN;
          return -1;
        }
      }
      return 1;
      break;
    case 't':
      if ("need to selftest")
      {
#ifdef WITH_SELFTEST
       main_test(optarg);
#endif
      }
      break;
    default:
      break;
      }
  return 0;
}
