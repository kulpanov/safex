#include "debug.h"
#include "gpios/GPIOConst.h"
#include "gpios/GPIOManager.h"
#include "utils/MySqlClient.h"
#include "module_init.h"

namespace safex {
    //pins for IO operations

    CInit::CInit() {
        //show logo
        VLOG(INFO) << "initialize" << LOGN;

        //1. Set GPIOs for inputs/outputs operations
        GPIO::GPIOManager* gp = GPIO::GPIOManager::getInstance();
        //export pin  as output
        pin_vH = GPIO::GPIOConst::getInstance()->getGpioByKey("P9_11");
        gp->exportPin(pin_vH);
        gp->setDirection(pin_vH, GPIO::OUTPUT);
        gp->setValue(pin_vH, GPIO::LOW);

        //export pin  as output
        pin_vN = GPIO::GPIOConst::getInstance()->getGpioByKey("P9_12");
        gp->exportPin(pin_vN);
        gp->setDirection(pin_vN, GPIO::OUTPUT);
        gp->setValue(pin_vN, GPIO::LOW);

        //export pin  as output
        pin_gc = GPIO::GPIOConst::getInstance()->getGpioByKey("P9_24");
        gp->exportPin(pin_gc);
        gp->setDirection(pin_gc, GPIO::OUTPUT);
        gp->setValue(pin_gc, GPIO::LOW);

//        //export pin P9_33 - AIN4 - as input
//        pin_AIN4 = GPIO::GPIOConst::getInstance()->getGpioByKey("P9_33");
//        gp->exportPin(pin_AIN4);
//        gp->setDirection(pin_AIN4, GPIO::INPUT);
//
//        //export pin P9_35 - AIN6 - as input
//        pin_AIN6 = GPIO::GPIOConst::getInstance()->getGpioByKey("P9_35");
//        gp->exportPin(pin_AIN6);
//        gp->setDirection(pin_AIN6, GPIO::INPUT);
//
//        //export pin P9_36 - AIN5- as input
//        pin_AIN5 = GPIO::GPIOConst::getInstance()->getGpioByKey("P9_36");
//        gp->exportPin(pin_AIN5);
//        gp->setDirection(pin_AIN5, GPIO::INPUT);

        //2. Connect to db and read last data
        safex::CMySqlClient* mysql = safex::CMySqlClient::getInstance();
        int res = 0;
        if (!mysql->isConnected()){
          res = mysql->connect("localhost");
          if (0 != res) {
            VLOG(ERRR) << mysql->getLastError() << LOGN;
            return ;
          }
        }
        res = mysql->push();//add record on start
        if (0 != res) {
          VLOG(ERRR) << mysql->getLastError() << LOGN;
          return ;
        }
    }

    CInit::~CInit() {
        VLOG(D1) << "release" << LOGN;
        GPIO::GPIOManager* gp = GPIO::GPIOManager::getInstance();
        gp->setValue(pin_vH, GPIO::LOW);//close valve
        gp->setValue(pin_vN, GPIO::LOW);//close valve
        gp->setValue(pin_gc, GPIO::HIGH);//shutdown the GC

        GPIO::GPIOManager::release();
        safex::CMySqlClient::release();
    }

    int CInit::execute() {
        VLOG(D1) << "refresh" << LOGN;
        GPIO::GPIOManager* gp = GPIO::GPIOManager::getInstance();
        safex::CMySqlClient* mysql = safex::CMySqlClient::getInstance();

        //pull data from mysql server, programm read last data, that has been written
        if (EOK != mysql->pull()) {
            VLOG(ERRR) << mysql->getLastError();
            return 1;
        }

        //3. Set values to hardwares, fix/add needed values
        gp->setValue(pin_vH, mysql->mValveH ? GPIO::HIGH : GPIO::LOW);
        gp->setValue(pin_vN, mysql->mValveN ? GPIO::HIGH : GPIO::LOW);

        return 0;
    }

}
