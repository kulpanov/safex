//============================================================================
// Name        : watchDog module
// Author      : Kulpanov (post@kulpanov.ru)
// Version     :
// Copyright   : Your copyright notice
// Description : module of safex

#include "../debug.h"

 #include <QtCore/QThreadPool>

//init module
namespace safex
{
  /** CWatchDog module.
   * Control flash LED, now
   */
  class CWatchDog: protected QRunnable
  {
  public:
    /** Init WD.
     * Setup LED, start the WD thread.
     * @return instance of WD
     */
    static CWatchDog* init();

    /** Release resources.
     *
     * @return
     */
    static CWatchDog* release();

    //add interface method here
    //...

  protected:
    ///runnable routine
    virtual void run();
    ///infloop flag, set false for exit
    bool loopFlag;

  private:
    //ctor and dtor are private, use init/release instead
    //
    CWatchDog();
    //
    ~CWatchDog();
    //can be only one
    static CWatchDog* instance;
  };//CWatchDog

} //safex
