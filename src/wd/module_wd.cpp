//============================================================================
// Name        : watchDog module
// Author      : Kulpanov (post@kulpanov.ru)
// Version     :
// Copyright   : Your copyright notice
// Description : module of safex

#include <iosfwd>
#include <iostream>     // std::cerr
#include <fstream>
#include <string>
#include <unistd.h>

#include "../debug.h"

#include "module_wd.h"

namespace safex {
//LED path
  #define LED_PATH "/sys/class/leds/beaglebone:green:usr0"
///WD timer period, mcs
  const int WD_Period = 1000 * 200; //200ms
///the trigger path
  const char* ledTrigger      = LED_PATH "/trigger";
///the brightness path
  const char* ledBrightness   = LED_PATH "/brightness";

  /** to remove the previously installed trigger
   */
  static void removeTrigger(){
    // remove the trigger from the LED
    std::fstream fs;
    fs.open( ledTrigger, std::fstream::out);
    fs << "none";
    fs.close();
  }

  //wd operation perform into a new thread
  void CWatchDog::run()
  {
    removeTrigger();

    std::ofstream fs;
    fs.open (ledBrightness);
    if(fs.fail()){
      VLOG(ERRR) << "Unable to open led brightness";
      return ;
    }

    bool isOn = false;
    do{
      if(isOn)
        fs << "255";//flash on
      else
        fs << "0";//flash off
      fs.flush();
      isOn = !isOn;
      usleep(WD_Period);
    }while(loopFlag);
    fs.close();
  }

  //constructor
  CWatchDog::CWatchDog():
      loopFlag(true){
    setAutoDelete(true);
  }


  //destructor
  CWatchDog::~CWatchDog(){
  }

  CWatchDog* CWatchDog::instance = NULL;

  CWatchDog* CWatchDog::init(){
    if(instance) return instance;

    instance = new CWatchDog;
    // QThreadPool takes ownership and deletes CWatchDog instance automatically
    QThreadPool::globalInstance()->start(instance);
    return instance;
  }

  CWatchDog* CWatchDog::release(){
    instance->loopFlag = false;
    //delete instance; - setAutoDelete(true) will do it
    return instance = NULL;
  }

}
