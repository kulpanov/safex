/*
 * debug.h
 *
 *  Created on: Jan 21, 2011
 *      Author: kulpanov
 */

#ifndef DEBUG_H_
#define DEBUG_H_

#include <errno.h>
#include <string.h>
//#include <glog/logging.h>
#include <QtCore/QDebug>
#include <iostream>//std C++ streams

extern int verboseLevel;
///Levels of log messages
enum {
  FAULT=0, ERRR, WARN, INFO, DET1, DET2, DET3, DET4
};

//log macro helpers
#define LOGS            this <<":"<<__func__

#define LOGP(p)         ","#p"=" << p
#define LOGPqs(p)       ","#p"=" << qPrintable(p)
#define LOGPs(p)       ","#p"=" << p.c_str()
#define LOGErr(e)       ", reason: "<< strerror(e)

#define LOGN            ""//std::endl
#define LOGE            ",catch:" << e.what()
#define LOG_E(lev)      VLOG(lev) << LOGS << LOGE << LOGN
#define VLOG(Level)     qDebug()//std::clog

#define TEST(expr)      if(! (expr)) VLOG(ERRR)<<"FAIL: " << #expr <<" at\n"<<__FILE__<<":"<<__LINE__<<LOGN;
#define FAIL(msg)       VLOG(ERRR)<<"FAIL: " << msg <<" at\n"<<__FILE__<<":"<<__LINE__<<LOGN;

#ifndef EOK
#define EOK 0
#endif

#endif /* DEBUG_H_ */
