#pragma once
#include "debug.h"
namespace safex {
    /** CInit module incapsulate init/release/refresh and other general operation.
     *
     */
    class CInit {
        public:
          /** Init.
             *  Connect to SQL, setup IO pins
             * @return 0-success
             */
             CInit();
             /** Release resources.
              * Release the SQL connection and BBB IO ports
              */
             ~CInit();

            /** refresh data from SQL.
             * Pull data from SQL, if need refresh - set IO values
             * @return 0-success
             */
            int execute();

private:
            //pins for IO operations
            ///BBB pin for valveH
            int pin_vH;
            //BBB pin for valveN
            int pin_vN;
            //GC shutdown command pin
            int pin_gc;
//            int pin_AIN4;
//            int pin_AIN5;
//            int pin_AIN6;
    };
}
