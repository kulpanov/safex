#pragma once
#include <unistd.h>
#include <QtCore/QtCore>
#include <QtCore/QObject>
#include <QtCore/QDateTime>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>

namespace safex {
#define DEF_SQL_PASS "chamin"
  //    enum class Heart_Module {
  //        H1,
  //        H2,
  //        H3
  //    };

  class CMySqlClient {
  private:
    static CMySqlClient* instance;  //must be only one
    QString mLastError;             ///last error string
    QSqlDatabase db;                ///database object

    CMySqlClient();
    ~CMySqlClient() {}
  public:
    QDateTime mTimeStamp;
    bool mSens1;
    bool mSens2;
    bool mSens3;
    bool mEmail;
    bool mShutdown;
    bool mStop;         //stop and exit
    bool mValveH;       //valve H - DO
    bool mValveN;       //valve N - DO
    bool mSaveH;
    bool eStop;

    //
    bool getH1(){return mSens1;}   //control bit for H1, now it's sens1
    bool getH2(){return mSens2;}   //control bit for H2, now it's sens2
    bool getH3(){return mSens3;}   //Control bit for H3, now it's sens3

    static CMySqlClient* getInstance(); //Use instead of constructor
    static CMySqlClient* release(); //Use instead of destructor

    /** Connect to database.
     *@param _aHost - hostname of server, currently is localhost
     *@param _aPort - port of server, curr is 3306
     *@param _aUserName - name of user of sql server, curr is root
     *@param _aPassword - pass of user of sql server, curr is ` (The grave accent)
     *@return sql error code, or 0 if success  */
    int connect(const QString& _aHost, int _aPort = 3306, const QString& _aUserName = "root", const QString& _aPassword = DEF_SQL_PASS);

    int createDataBase(); //Initialize database and tables. Return 0 if success, error code otherwise.
    int pull(); //Pull latest data from database. Return 0 if success, error code otherwise.
    int push(); //Push current data to database. Return 0 if sucess, error code otherwise.
    //            QSqlQuery heart_table_last(Heart_Module);
    //            int heart_table_append(Heart_Module, int, int);

    /** Get last data from H1 table.
     * @param _lastTime timestamp of data
     * @param _lastValue value
     * @return sql error, 0=success   */
    int getData_H1(QDateTime& _lastTime, int& _lastValue);

    /** Add data to H1 table
     * @param _value value of equation
     * @param _temperature temperature from sensor
     * @return sql error, 0=success   */
    int addData_H1(int _value, int _temperature);

    /** Get last data from H2 table.
     * @param _lastTime timestamp of data
     * @param _lastValue value
     * @return sql error, 0=success   */
    int getData_H2(QDateTime& _lastTime, int& _lastValue);

    /** Add data to H2 table
     * @param _value value
     * @return sql error, 0=success   */
    int addData_H2(int _value);

    /** Replace data to H3 table
     * @param _value value
     * @return sql error, 0=success   */
    int replaceData_H3(int _value);

    /** Set the emerg stop flag
     * @param _value value
     * @return sql error, 0=success   */
    int setEStop(bool _value);

    ///Return connected db
    QSqlQuery getQuery(){return QSqlQuery(db);}

    QString getLastError() { return mLastError; }

    bool isConnected() { return db.isOpen(); }
  };
}
