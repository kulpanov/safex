#include <libio.h>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QtCore/QVariant>
#include "MySqlClient.h"
#include <QtSql/QSqlDatabase>
namespace safex {
    CMySqlClient::CMySqlClient() {
        mTimeStamp = QDateTime::currentDateTime();
        mSens1 = true;
        mSens2 = true;
        mSens3 = false;
        mEmail = false;
        mShutdown = false;
        mStop = false;
        mValveH = false;
        mValveN = false;
        mSaveH = true;
        eStop = false;
//        mH1 = false;
//        mH2 = false;
//        mH3 = false;
    }

    int CMySqlClient::connect(const QString& _aHost, int _aPort, const QString& _aUserName, const QString& _aPassword) {
        //set type of db: MYSQL
        db = QSqlDatabase::addDatabase("QMYSQL");
        //set params of connection and db
        db.setHostName(_aHost);
        db.setUserName(_aUserName);
        db.setPassword("chemin");
        db.setPort(_aPort);
        db.setDatabaseName("safex");
        //open!
        if (!db.open())
        {//smthng wrong, return errorcode
            mLastError = db.lastError().text();
            return db.lastError().number();
        }
        else
        {//ok
            return 0;
        }
    }

    int CMySqlClient::createDataBase() {
        //db = QSqlDatabase::database();
        int res = 0;
        db = QSqlDatabase::addDatabase("QMYSQL");
        //set params of connection and db
        db.setHostName("localhost");
        db.setUserName("root");
        db.setPassword("chemin");
        db.setPort(3306);
        db.open();
        //create the sql query
        QSqlQuery q(db);
        if (!q.exec("DROP DATABASE safex;") && q.lastError().number() != 1007) {//smthng wrong, return errorcode
//            mLastError = q.lastError().text(); - maybe it doesn't exist
//            return db.lastError().number();
        }
        //create database
        if (!q.exec("CREATE DATABASE safex;") && q.lastError().number() != 1007) {//smthng wrong, return errorcode
            mLastError = q.lastError().text();
            return db.lastError().number();
        }
        db.close();

        //open db and create table
        db.setDatabaseName("safex");
        db.open();
        q = QSqlQuery(db);

        if (!q.exec("CREATE TABLE initialize(idnr INTEGER AUTO_INCREMENT PRIMARY KEY"
            ", time_stamp DATETIME"
            ", sens_1 BOOL NOT NULL DEFAULT 1, sens_2 BOOL NOT NULL DEFAULT 1, sens_3 BOOL NOT NULL DEFAULT 0"
            ", e_mail BOOL NOT NULL DEFAULT 0, c_shutdown BOOL NOT NULL DEFAULT 0, c_stop BOOL NOT NULL DEFAULT 0"
            ", valve_h BOOL NOT NULL DEFAULT 0, valve_n BOOL NOT NULL DEFAULT 0, save_h BOOL NOT NULL DEFAULT 1"
            ", emergency_stop BOOL DEFAULT 0"
            ");")
            && q.lastError().number() != 1050) {
            mLastError = q.lastError().text();
            return q.lastError().number();
        }
        res = push();
        if(0 != res) return res;


        if (!q.exec("CREATE TABLE H_1(idnr INTEGER AUTO_INCREMENT PRIMARY KEY, time_stamp DATETIME"
            ", value INTEGER, temperature INTEGER);") && q.lastError().number() != 1050) {
            mLastError = q.lastError().text();
            return q.lastError().number();
        }
        res = addData_H1(0, 0);
        if(0 != res) return res;

        if (!q.exec("CREATE TABLE H_2(idnr INTEGER AUTO_INCREMENT PRIMARY KEY, time_stamp DATETIME"
            ", value INTEGER);") && q.lastError().number() != 1050) {
            mLastError = q.lastError().text();
            return q.lastError().number();
        }
        res = addData_H2(0);
        if(0 != res) return res;

        if (!q.exec("CREATE TABLE H_3(idnr INTEGER AUTO_INCREMENT PRIMARY KEY, time_stamp DATETIME"
            ", value INTEGER);") && q.lastError().number() != 1050) {
            mLastError = q.lastError().text();
            return q.lastError().number();
        }
        res = replaceData_H3(0);
        if(0 != res) return res;

        return 0;
    }

    int CMySqlClient::pull() {
        QSqlQuery q(db);
        //select query for pull
        if (!q.exec( "SELECT time_stamp, sens_1, sens_2, sens_3, e_mail, c_shutdown, c_stop, valve_h, valve_n, save_h, emergency_stop "
            "FROM initialize ORDER BY idnr DESC LIMIT 1;") || !q.first()) {
            mLastError = q.lastError().text();
            return q.lastError().number();
        }
        else {//check for a new date and copy data to fields
            QDateTime newDateTime = q.value(0).toDateTime();
            if(mTimeStamp < newDateTime){
                mTimeStamp = newDateTime;
                mSens1 = q.value(1).toBool();
                mSens2 = q.value(2).toBool();
                mSens3 = q.value(3).toBool();
                mEmail = q.value(4).toBool();
                mShutdown = q.value(5).toBool();
                mStop = q.value(6).toBool();
                mValveH = q.value(7).toBool();
                mValveN = q.value(8).toBool();
                mSaveH = q.value(9).toBool();
                eStop = q.value(10).toBool();
            }
            return 0;
        }
    }

    int CMySqlClient::push() {
        QSqlQuery q(db);
        //insert query for push
        if (!q.prepare( "INSERT INTO initialize VALUES(NULL, :time, :sens_1, :sens_2, :sens_3"
            ", :e_mail, :c_shutdown, :c_stop, :valve_h, :valve_n, :save_h, :emergency_stop);")) {
            mLastError = q.lastError().text();
            return q.lastError().number();
        }
        //copy data to query
        q.bindValue(":time", mTimeStamp.toString("yyyy-MM-dd hh:mm:ss"));
        q.bindValue(":sens_1", mSens1);
        q.bindValue(":sens_2", mSens2);
        q.bindValue(":sens_3", mSens3);
        q.bindValue(":e_mail", mEmail);
        q.bindValue(":c_shutdown", mShutdown);
        q.bindValue(":c_stop", mStop);
        q.bindValue(":valve_h", mValveH);
        q.bindValue(":valve_n", mValveN);
        q.bindValue(":save_h", mSaveH);
        q.bindValue(":emergency_stop", eStop);

        if (!q.exec()) {
            mLastError = q.lastError().text();
            return q.lastError().number();
        }
        return 0;
    }


    int CMySqlClient::getData_H1(QDateTime& _lastTime, int& _lastValue){
      QSqlQuery q(db);
      if(!q.exec("SELECT time_stamp, value FROM H_1 ORDER BY time_stamp DESC LIMIT 1;") || !q.first())
      {
        mLastError = q.lastError().text();
        return q.lastError().number();
      }
      _lastTime = q.value(0).toDateTime();
      _lastValue = q.value(1).toInt();
      return 0;
    }

    int CMySqlClient::addData_H1(int _value, int _temperature){
      QSqlQuery q(db);
      if (!q.prepare( "INSERT INTO H_1 VALUES(NULL, :time, :value, :temperature);"))
      {
        mLastError = q.lastError().text();
        return q.lastError().number();
      }
      q.bindValue(":time", QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));//timestamp
      q.bindValue(":value", _value);     //result of the equation
      q.bindValue(":temperature", _temperature);//the temperature of AIN5
      if (!q.exec()) {
        mLastError = q.lastError().text();
        return q.lastError().number();
      }
      return 0;
    }

    int CMySqlClient::getData_H2(QDateTime& _lastTime, int& _lastValue){
      QSqlQuery q(db);
      if(!q.exec("SELECT time_stamp, value FROM H_2 ORDER BY time_stamp DESC LIMIT 1;") || !q.first())
      {
        mLastError = q.lastError().text();
        return q.lastError().number();
      }
      _lastTime = q.value(0).toDateTime();
      _lastValue = q.value(1).toInt();
      return 0;
    }

    int CMySqlClient::addData_H2(int _value){
      QSqlQuery q(db);
      if (!q.prepare( "INSERT INTO H_2 VALUES(NULL, :time, :value);"))
      {
        mLastError = q.lastError().text();
        return q.lastError().number();
      }
      q.bindValue(":time", QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));//timestamp
      q.bindValue(":value", _value);     //result of the equation
      if (!q.exec()) {
        mLastError = q.lastError().text();
        return q.lastError().number();
      }
      return 0;
    }

    int CMySqlClient::replaceData_H3(int _value){
      QSqlQuery q(db);

      if(!q.exec("DELETE * FROM H_3 VALUES;")) {
          mLastError = q.lastError().text();
      }
      if (!q.prepare( "INSERT INTO H_3 VALUES(NULL, :time, :value);")) {
          mLastError = q.lastError().text();
          return q.lastError().number();
      }
      q.bindValue(":time", QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));//timestamp
      q.bindValue(":value", _value);
      if (!q.exec()) {
        mLastError = q.lastError().text();
        return q.lastError().number();
      }
      return 0;
    }

    int CMySqlClient::setEStop(bool _value){
      if(eStop != _value){
        eStop = _value;
        return push();
      }
      return 0;
    }

    CMySqlClient* CMySqlClient::instance = NULL;

    CMySqlClient* CMySqlClient::getInstance() {
        if(instance) return instance;
        return instance = new CMySqlClient;
    }

    CMySqlClient* CMySqlClient::release(){
        delete instance;
        return instance = NULL;
    }
}
