#include "heart_beat.h"
#include "./utils/MySqlClient.h"
#include "debug.h"
#include "module_init.h"
#include "gpios/GPIOConst.h"
#include "gpios/GPIOManager.h"
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QtCore/QVariant>
#include <QtCore/QDateTime>

namespace safex {

  //ADC's path
#define ADC_PATH "/sys/bus/platform/devices/tiadc/iio:device0/"

  const char* adc4      = ADC_PATH "in_voltage4_raw";
  const char* adc5      = ADC_PATH "in_voltage5_raw";
  const char* adc6      = ADC_PATH "in_voltage6_raw";

  CHeartBeat::CHeartBeat():
         fs_adc4(adc4),fs_adc5(adc5),fs_adc6(adc6)
  {
    gp = GPIO::GPIOManager::getInstance();
    mysql = safex::CMySqlClient::getInstance();

    //open files of adcs and setup streams
    if(! fs_adc4.open(QIODevice::ReadOnly)){
      VLOG(ERRR) << "Unable to open adc4";
    }
    if(! fs_adc5.open(QIODevice::ReadOnly)){
      VLOG(ERRR) << "Unable to open adc5";
    }
    if(! fs_adc6.open(QIODevice::ReadOnly)){
      VLOG(ERRR) << "Unable to open adc6";
    }
    ds_adc4.setDevice(&fs_adc4);
    ds_adc5.setDevice(&fs_adc5);
    ds_adc6.setDevice(&fs_adc6);
    //init adcs values
    ain4 = 0; ain5 = 0; ain6 = 0;

    pin_alert = GPIO::GPIOConst::getInstance()->getGpioByKey("P9_22");
    gp->exportPin(pin_alert);
    gp->setDirection(pin_alert, GPIO::OUTPUT);
  }

  ///check range of ADC:  a range of +- _percent%
  static bool checkRange(int _base_v, int equ_v, double _percent){
    if(_base_v == equ_v) return true;//for zero case
    int value_high = _base_v *  (_percent + 1);
    int value_low  = _base_v * -(_percent - 1);
    if (equ_v > value_low || equ_v < value_high)
      return true;//it is in a range of +- 3% of the new value do nothing
    return false;
  }

  ///macro:check result and log&return if error occurs
#define _TRY(expr)  {int res = (expr); if(0 != res){\
    VLOG(WARN) << mysql->getLastError() << LOGN;\
    return res;} }

  //perform the H1 module actions
  int CHeartBeat::processH1()
  {
    VLOG(INFO) << "the H1 module" << LOGN;

    int value = (ain4 * (1 + ain5)) / 100; //dummy equation!

    //step1: open the table H_1 (order of timestamp descending), get the timestamp and value from row 1.
    QDateTime lastTime; int lastValue;
    _TRY( mysql->getData_H1(lastTime, lastValue));

    //step2: check time and add/modify values
    unsigned int difference = QDateTime::currentDateTime().toTime_t() - lastTime.toTime_t();
    if (difference > 60 || !checkRange(lastValue, value, 0.03))
    { //it is older then 1 minute, or it is NOT a range of +- 3% of the new value
      //append a new row and insert the actual values
      VLOG(DET1) << "H1: add data" << value << ":" << ain5 <<LOGN;
      _TRY(mysql->addData_H1(value, ain5));
    }

    return 0;
  }
  //perform the H2 module actions
  int CHeartBeat::processH2()
  {
    VLOG(INFO) << "the H2 module" << LOGN;
    //step1: open the table H_2, get the timestamp and value from row 1.
    QDateTime lastTime;
    int lastValue;
    _TRY(mysql->getData_H2(lastTime, lastValue));
    //step2: check time and add/modify values
    unsigned int difference = QDateTime::currentDateTime().toTime_t()
            - lastTime.toTime_t();
    if (difference > 60 || !checkRange(lastValue, ain6, 0.03))
    { //it is older then 1 minute, or it is NOT a range of +- 3% of the new value
      //append a new row and insert the actual values
      VLOG(DET1) << "H2: add data" << ain6 <<LOGN;
      _TRY(mysql->addData_H2(ain6));
    }
    return 0;
  }

  //perform the H3 module actions
  int CHeartBeat::processH3(){
    VLOG(INFO) << "the H3 module" << LOGN;
    VLOG(DET1) << "H3: add data" << ain6 <<LOGN;
    _TRY(mysql->replaceData_H3(ain6));
    return 0;
  }


  int
  CHeartBeat::execute()
  {
    { //read datas from adcs
      // 2275 = ((4096 - 1 )/ 1.8); - digit to voltage equation, from TI manual.
      //processors.wiki.ti.com/index.php/AM335x_ADC_Driver%27s_Guide
      //all results in mV
      ds_adc4 >> ain4;  ds_adc4.seek(0);
      ain4 = ain4 * 1000 / 2275;
      ds_adc5 >> ain5;  ds_adc5.seek(0);
      ain5 = ain5 * 1000 / 2275;
      ds_adc6 >> ain6;  ds_adc6.seek(0);
      ain6 = ain6 * 1000 / 2275;
      //If the value of H1 or H2 greater then e.g. 1500, the alert port must be set. Otherwise under 1500 it will be cleared.
      //The port is GPIO_3 P8
      if(ain4>1500)
        gp->setValue(pin_alert, GPIO::HIGH);
      else if(ain6>1500)
        gp->setValue(pin_alert, GPIO::HIGH);
      else
        gp->setValue(pin_alert, GPIO::LOW);
    }
    if (mysql->getH1())
    {
      processH1();
    }
    if (mysql->getH2())
    {
      processH2();
    }
    if (mysql->getH3())
    {
      processH3();
    }
    if (mysql->mStop)
    {
      VLOG(INFO) << "Stopping" << LOGN;
      return 1;
    }
    return 0;
  }

}
