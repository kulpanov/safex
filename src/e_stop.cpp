#include "debug.h"
#include "e_stop.h"
#include "./utils/MySqlClient.h"
#include "module_init.h"
#include "gpios/GPIOConst.h"
#include "gpios/GPIOManager.h"


namespace safex {

 //ADC's path
  CEmergencyStop::CEmergencyStop()
  {
    gp = GPIO::GPIOManager::getInstance();
    mysql = safex::CMySqlClient::getInstance();

    //open files of adcs and setup streams
    pin_stop = GPIO::GPIOConst::getInstance()->getGpioByKey("P8_26");
    gp->exportPin(pin_stop);
    gp->setDirection(pin_stop, GPIO::INPUT);
    pin_stop_value = 0;
  }

  CEmergencyStop::~CEmergencyStop(){

  }

  int CEmergencyStop::execute()
  {
    if(1 == gp->getValue(pin_stop)){
      if(1 == pin_stop_value){
        mysql->setEStop(true);//set emergency_stop field of initilize
        VLOG(INFO) << "Emergency stop" << LOGN;
        return 1;
      }
      pin_stop_value = 1;
    }else pin_stop_value = 0;
    return 0;
  }

}
